FROM alpine:latest

RUN apk update
RUN apk add rust cargo libgit2 curl openssl openssl-dev git

WORKDIR /build
